<?php


namespace App\Common;


interface Collection extends \Iterator, \Countable
{
}