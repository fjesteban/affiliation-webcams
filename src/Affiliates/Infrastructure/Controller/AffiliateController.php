<?php

namespace App\Affiliates\Infrastructure\Controller;
use App\Affiliates\Application\WebcamsList\AffiliateWebcamsListHandler;
use App\Affiliates\Application\WebcamsList\AffiliateWebcamsListQuery;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;

class AffiliateController
{
    /**
     * @var AffiliateWebcamsListHandler
     */
    private $applicationService;
    /**
     * @var Environment
     */
    private $twig;

    public function __construct(AffiliateWebcamsListHandler $applicationService, Environment $twig)
    {
        $this->applicationService = $applicationService;
        $this->twig = $twig;
    }

    public function __invoke(string $affiliateId, int $page){
        return new Response(
            $this->twig->render(
                'affiliates/webcams.twig',
                [
                    'affiliation' => $this->applicationService->__invoke(new AffiliateWebcamsListQuery($affiliateId, $page)),
                    'currentPage' => $page
                ]
            )
        );

    }
}