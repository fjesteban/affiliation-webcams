<?php

namespace App\Affiliates\Infrastructure\Persistence;

use App\Affiliates\Domain\Affiliate;
use App\Affiliates\Domain\AffiliateCollection;
use App\Affiliates\Domain\AffiliateId;
use App\Affiliates\Domain\AffiliateRepository;

class JsonFileAffiliateRepository implements AffiliateRepository
{

    private $affiliateCollection;

    public function __construct()
    {
        $this->affiliateCollection = new AffiliateCollection();
        $jsonContent = file_get_contents(__DIR__.'/affiliates.json', 'r');
        $affiliates = json_decode($jsonContent, true);
        foreach( $affiliates as $affiliate) {
            $this->affiliateCollection->add(
                $this->hydrate($affiliate)
            );
        }
    }

    public function find(AffiliateId $affiliateId): ?Affiliate
    {
        /** @var Affiliate $affiliate */
        foreach ($this->affiliateCollection as $affiliate)
        {
            if ($affiliateId->isEqual($affiliate->getAffiliateId())) {
                return $affiliate;
            }
        }
        return null;
    }

    private function hydrate(array $affiliateArray): Affiliate
    {
        return new Affiliate(
            new AffiliateId($affiliateArray['webcam-NATS']),
            $affiliateArray['name'],
            $affiliateArray['url'],
            $affiliateArray['NATS'],
            $affiliateArray['CSS-path'],
            $affiliateArray['Google-Analytics-id']
        );
    }
}
