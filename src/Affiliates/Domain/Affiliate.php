<?php

namespace App\Affiliates\Domain;

class Affiliate
{
    /**
     * @var AffiliateId
     */
    private $affiliateId;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $url;
    /**
     * @var string
     */
    private $cumlouderNats;
    /**
     * @var string
     */
    private $cssPath;
    /**
     * @var string
     */
    private $googleAnalitysIdentifier;

    public function __construct(
        AffiliateId $affiliateId,
        string $name,
        string $url,
        string $cumlouderNats,
        string $cssPath,
        string $googleAnalitysIdentifier
    )
    {

        $this->affiliateId = $affiliateId;
        $this->name = $name;
        $this->url = $url;
        $this->cumlouderNats = $cumlouderNats;
        $this->cssPath = $cssPath;
        $this->googleAnalitysIdentifier = $googleAnalitysIdentifier;
    }

    /**
     * @return AffiliateId
     */
    public function getAffiliateId(): AffiliateId
    {
        return $this->affiliateId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getCumlouderNats(): string
    {
        return $this->cumlouderNats;
    }

    /**
     * @return string
     */
    public function getCssPath(): string
    {
        return $this->cssPath;
    }

    /**
     * @return string
     */
    public function getGoogleAnalitysIdentifier(): string
    {
        return $this->googleAnalitysIdentifier;
    }


}
