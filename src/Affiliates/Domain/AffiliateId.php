<?php

namespace App\Affiliates\Domain;

class AffiliateId
{
    /**
     * @var String
     */
    private $affiliateId;

    public function __construct(String $affiliateId)
    {

        $this->affiliateId = $affiliateId;
    }

    public function getValue(): string
    {
        return $this->affiliateId;
    }

    public function isEqual(self $anotherAffiliateId): bool
    {
        return $this->getValue() === $anotherAffiliateId->getValue();
    }
}
