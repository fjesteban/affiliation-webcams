<?php


namespace App\Affiliates\Domain;


interface AffiliateRepository
{
    public function find(AffiliateId $affiliateId): ?Affiliate;
}