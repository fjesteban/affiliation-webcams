<?php

namespace App\Affiliates\Domain;

use App\Common\Collection;
use App\Common\CollectionTrait;

class AffiliateCollection implements Collection
{
    use CollectionTrait;

    public function add(Affiliate $affiliate): void
    {
        $this->items[] = $affiliate;
        $this->rewind();
    }
}
