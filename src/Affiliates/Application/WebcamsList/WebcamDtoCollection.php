<?php

namespace App\Affiliates\Application\WebcamsList;

use App\Common\Collection;
use App\Common\CollectionTrait;

class WebcamDtoCollection implements Collection
{
    use CollectionTrait;

    public function add(WebcamDto $dto): void
    {
        $this->items[] = $dto;
        $this->rewind();
    }
}
