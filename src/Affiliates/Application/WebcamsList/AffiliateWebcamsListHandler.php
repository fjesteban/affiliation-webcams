<?php

namespace App\Affiliates\Application\WebcamsList;

use App\Affiliates\Domain\AffiliateId;
use App\Affiliates\Domain\AffiliateRepository;
use App\Webcam\Domain\Webcam;
use App\Webcam\Domain\WebcamRepository;

class AffiliateWebcamsListHandler
{
    /**
     * @var AffiliateRepository
     */
    private $affiliateRepository;
    /**
     * @var WebcamRepository
     */
    private $webcamRepository;

    public function __construct(AffiliateRepository $affiliateRepository, WebcamRepository $webcamRepository)
    {

        $this->affiliateRepository = $affiliateRepository;
        $this->webcamRepository = $webcamRepository;
    }

    public function __invoke(AffiliateWebcamsListQuery $query):AffiliationDto
    {
        $affiliate = $this->affiliateRepository->find(new AffiliateId($query->getAffiliateId()));
        $webcams = $this->webcamRepository->findAll($query->getPage());

        $webcamDtoCollection = new WebcamDtoCollection();

        /** @var Webcam $webcam */
        foreach ($webcams as $webcam) {
            $webcamDtoCollection->add(new WebcamDto(
                    $webcam->getName(),
                    $webcam->getPermalink(),
                    $webcam->getBigThumbnail(),
                    $webcam->getSmallThumbnail()
                )
           );
        }

        return new AffiliationDto(
            $affiliate->getName(),
            $affiliate->getAffiliateId()->getValue(),
            $affiliate->getCssPath(),
            $webcamDtoCollection
        );
    }
}
