<?php

namespace App\Affiliates\Application\WebcamsList;

class WebcamDto
{
    private $name;
    private $permalink;

    /**
     * @var string
     */
    private $bigThumbnail;
    /**
     * @var string
     */
    private $smallThumbnail;

    public function __construct(string $name, string $permalink, string $bigThumbnail, string $smallThumbnail)
    {

        $this->name = $name;
        $this->permalink = $permalink;
        $this->bigThumbnail = $bigThumbnail;
        $this->smallThumbnail = $smallThumbnail;
    }

    public function name()
    {
        return $this->name;
    }

    public function url()
    {
        return $this->permalink;
    }

    /**
     * @return string
     */
    public function bigThumbnail(): string
    {
        return $this->bigThumbnail;
    }

    /**
     * @return string
     */
    public function smallThumbnail(): string
    {
        return $this->smallThumbnail;
    }


}
