<?php

namespace App\Affiliates\Application\WebcamsList;

class AffiliationDto
{
    private $affiliateNats;
    private $cssPath;
    private $webcamCollection;
    /**
     * @var string
     */
    private $name;

    public function __construct(string $name, string $affiliateNats, string $cssPath, WebcamDtoCollection $webcamCollection)
    {
        $this->affiliateNats = $affiliateNats;
        $this->cssPath = $cssPath;
        $this->webcamCollection = $webcamCollection;
        $this->name = $name;
    }

    public function name():string
    {
        return $this->name;
    }

    public function nats(): string
    {
        return $this->affiliateNats;
    }

    public function cssPath(): string
    {
        return $this->cssPath;
    }

    public function webcams(): WebcamDtoCollection
    {
        return $this->webcamCollection;
    }

}
