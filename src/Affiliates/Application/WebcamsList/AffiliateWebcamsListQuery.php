<?php

namespace App\Affiliates\Application\WebcamsList;


class AffiliateWebcamsListQuery
{
    /**
     * @var string
     */
    private $affiliateId;


    public function __construct(string $affiliateId, int $page)
    {
        $this->affiliateId = $affiliateId;

        $this->page = $page;
    }

    /**
     * @return string
     */
    public function getAffiliateId(): string
    {
        return $this->affiliateId;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }



}
