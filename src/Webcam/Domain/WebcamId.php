<?php

namespace App\Webcam\Domain;

class WebcamId
{
    /**
     * @var int
     */
    private $webcamId;

    public function __construct(int $webcamId)
    {
        $this->webcamId = $webcamId;
    }

    public function getValue(): int
    {
        return $this->webcamId;
    }

    public function isEqual(self $webcamId)
    {
        return $this->getValue() === $webcamId->getValue();
    }
}
