<?php

namespace App\Webcam\Domain;

class Webcam
{
    /**
     * @var WebcamId
     */
    private $webcamId;
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $permalink;
    /**
     * @var string
     */
    private $smallThumbnail;
    /**
     * @var string
     */
    private $bigThumbnail;

    public function __construct(
        WebcamId $webcamId,
        string $name,
        string $permalink,
        string $smallThumbnail,
        string $bigThumbnail
    )
    {
        $this->webcamId = $webcamId;
        $this->name = $name;
        $this->permalink = $permalink;
        $this->smallThumbnail = $smallThumbnail;
        $this->bigThumbnail = $bigThumbnail;
    }

    /**
     * @return WebcamId
     */
    public function getWebcamId(): WebcamId
    {
        return $this->webcamId;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPermalink(): string
    {
        return $this->permalink;
    }

    /**
     * @return string
     */
    public function getSmallThumbnail(): string
    {
        return $this->smallThumbnail;
    }

    /**
     * @return string
     */
    public function getBigThumbnail(): string
    {
        return $this->bigThumbnail;
    }


}
