<?php


namespace App\Webcam\Domain;


interface WebcamRepository
{
    public function findAll(int $page): WebcamCollection;
}