<?php

namespace App\Webcam\Domain;

use App\Common\Collection;
use App\Common\CollectionTrait;

class WebcamCollection implements Collection
{
    use CollectionTrait;

    public function add(Webcam $webcam): void
    {
        $this->items[] = $webcam;
        $this->rewind();
    }
}
