<?php

namespace App\Webcam\Infrastructure;

use App\Webcam\Domain\Webcam;
use App\Webcam\Domain\WebcamCollection;
use App\Webcam\Domain\WebcamId;
use App\Webcam\Domain\WebcamRepository;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class ApiWebcamRepository implements WebcamRepository
{
    private $webcamCollection;
    /**
     * @var HttpClientInterface
     */
    private $httpClient;
    /**
     * @var CacheInterface
     */
    private $cache;
    private $apiEndpoint;

    /**
     * ApiWebcamRepository constructor.
     * @param string $apiEndpoint
     * @param HttpClientInterface $httpClient
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function __construct( HttpClientInterface $httpClient, CacheInterface $cache)
    {
        $this->apiEndpoint = $_ENV['CUMLOUDER_WEBCAMS_URL'];
        $this->httpClient = $httpClient;
        $this->cache = $cache;
    }

    public function findAll(int $page = 1): WebcamCollection
    {
        $httpClient = $this->httpClient;
        $apiEndpoint = "{$this->apiEndpoint}/{$page}/";
        $webcamsList = $this->cache->get('api-webcams-list'.$page, function (ItemInterface $item) use ($httpClient, $apiEndpoint) {
            $item->expiresAfter(900);
            $apiResponse =  $httpClient->request('GET', $apiEndpoint)->getContent();
            return json_decode($apiResponse, true);
        });

        $this->webcamCollection = new WebcamCollection();
        foreach ($webcamsList as $webcam) {
            $this->webcamCollection->add(
                $this->hydrate($webcam)
            );
        }
        return $this->webcamCollection;
    }


    public function hydrate(array $item): Webcam
    {
        return new Webcam(
            new WebcamId($item['wbmerId']),
            $item['wbmerNick'],
            $item['wbmerPermalink'],
            $item['wbmerThumb2'],
            $item['wbmerThumb3']
        );
    }
}
