<?php

namespace App\Tests;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class WebcamTest extends WebTestCase
{

    public function test_It_will_render_sixty_webcams()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/webcams/MjYwNS4xLjI1LjQzLjAuMC4wLjAuMA/1');
        $this->assertEquals(
            60,
            $crawler->filter('div.chica')->count()
        );
    }

    public function test_It_will_render_five_big_thumbnails()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/webcams/MjYwNS4xLjI1LjQzLjAuMC4wLjAuMA/1');
        $this->assertEquals(
            5,
            $crawler->filter('div.chica-grande')->count()
        );
    }

    public function test_It_will_render_fisrt_big_thumbnail_once_and_rest_twice()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/webcams/MjYwNS4xLjI1LjQzLjAuMC4wLjAuMA/1');

        $firstWebcamName = $crawler->filter('div.chica-grande')->first()->text();
        $allWebcamsNames = $crawler->filter('div.chica')->each(function (Crawler $node, $i) {
            return $node->text();
        });

        $crawler->filter('div.chica-grande')->each(function (Crawler $node) use ($firstWebcamName, $allWebcamsNames){
            $expectedRenders = $node->text() === $firstWebcamName ? 1 : 2;
            $this->assertEquals(
                $expectedRenders,
                array_count_values($allWebcamsNames)[$node->text()]
            );
        });
    }
}
