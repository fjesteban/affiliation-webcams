### Instalación

```
git clone https://gitlab.com/fjesteban/affiliation-webcams.git  
cd affiliation-webcams
docker-compose up -d
docker-compose exec php-fpm composer install  
```

### Test
```
docker-compose exec php-fpm ./bin/phpunit 
```

### Ejemplos

```
http://localhost:6090/webcams/MjYwNS4xLjI1LjQzLjAuMC4wLjAuMA
http://localhost:6090/webcams/da22e451ad8553a72760ec39325352dc
http://localhost:6090/webcams/38a07c303119dd2345081a698f3e0a83
``